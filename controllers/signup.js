const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const sleep = require('util').promisify(setTimeout);
const userservices = require('../services/userServices');
var app1 = express();


function now() {
  let curdate = new Date();
  return curdate.getTime() + curdate.getTimezoneOffset() * 60000;
}

/// add test ======

//=============== user registration ==========
app1.post('/user_signup', async (req, res, next) => {
  let obj;
  let required = [];
  let emailFlag = 0;
  let phoneFlag = 0;
  let submitFlag = 0;

  if (!req.body.type) required.push("type");
  if (!req.body.first_name) required.push("first_name");
  if (!req.body.last_name) required.push("last_name");
  if (!req.body.password) required.push("password");
  if (!req.body.gender) required.push("gender");
  // if (!req.body.countrycode) required.push("countrycode");

  if(req.body.type && req.body.type == "both")
  {
      if (!req.body.email) required.push("email");
      if (!req.body.phone) required.push("phone");
  }
  else if(req.body.type && req.body.type == "phone")
  {
      if (!req.body.phone) required.push("phone");
  }
  else if(req.body.type && req.body.type == "email")
  {
      if (!req.body.email) required.push("email");
  }
  
  if (required.length === 0) {
      let type = req.body.type;
      let email = "";
      let phone = "";


let date_ob = new Date();
let date = ("0" + date_ob.getDate()).slice(-2);
let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
let year = date_ob.getFullYear();
let hours = date_ob.getHours();
let minutes = date_ob.getMinutes();
let seconds = date_ob.getSeconds();
let finaldt = year + "-" + month + "-" + date + " " + hours + ":" + minutes + ":" + seconds;


          if(req.body.email)
          {
              let uservalueemail = {
                  email: req.body.email
              }
              let getUserByEmail = await userservices.findUser(uservalueemail, "email");
              // console.log(getUserByEmail);
              if (getUserByEmail) {
                if (getUserByEmail.status == "success") {
                    obj = { status: "fail", message: "email already exist", response: "" };
                }
                else {
                    emailFlag = 1;
                }
              }
              else {
                  emailFlag = 1;
              }
          }

          //=================================================
          if(req.body.phone)
          {
              let uservaluephone = {
                  phone: req.body.phone
              }
              let getUserByPhone = await userservices.findUser(uservaluephone, "phone");
              // console.log(getUserByPhone);
              if (getUserByPhone.status == "success") {
                  obj = { status: "fail", message: "phone already exist", response: "" };
              }
              else {
                  phoneFlag = 1;
              }
          }
          // return;
          //=================================================
          if (emailFlag == 1 && phoneFlag == 1 && req.body.type == "both") {
              email = req.body.email;
              phone = req.body.phone;
              submitFlag = 1;
          } else if (emailFlag == 1 && req.body.type == "email") {
              email = req.body.email;
              submitFlag = 1;
          } else if (phoneFlag == 1 && req.body.type == "phone") {
              phone = req.body.phone;
              submitFlag = 1;
          }
          //=================================================
          if (submitFlag == 1) {
            let userobj = {
              num_master_id: "3",
              first_name: req.body.first_name,
              last_name: req.body.last_name,
              txt_user_id: email,
              txt_password: req.body.password,
              txt_gender: req.body.gender,    
              txt_emailid: email,
              num_mobile_no: phone,
              dat_creation_date: finaldt,
              txt_active: "1"
          };
        //   console.log(userobj);return;
              let getUser = await userservices.addUser(userobj);
              // console.log(getUser);
              if (getUser.status == "success") {
                  // //====================================
                  // if (req.body.email) {
                  //     let content = "User registration successfull";
                  //     obj = {
                  //         to: req.body.email,
                  //         content: content,
                  //         subject: "User Registration",
                  //       };
                  //     let resemil = await addmail.sendNewMail(obj);
                  // }
                  // //=========================================================================
                  // if (req.body.phone) {
                  //     let smsContent = "User registration successfull";
                  //     let sendSMS = await sms.sendSMS(req.body.phone, smsContent);
                  // }
                  //===========================================================================
                  obj = { status: "success", message: "User registration successfully",response:getUser.result };
              } else {
                  obj = {
                      status: "fail",
                      message: "Sorry! An unknown error occurred"
                  };
              }
          } else {
              if (req.body.type == "both") {
                  obj = {
                      status: "fail",
                      message: "Sorry! email or phone already exist"
                  };
              } else if (req.body.type == "email") {
                  obj = {
                      status: "fail",
                      message: "Sorry! email already exist"
                  };
              } else if (req.body.type == "phone") {
                  obj = {
                      status: "fail",
                      message: "Sorry! phone already exist"
                  };
              }
          }
  } else {
      let message = required.map((item) => {
          return " " + item;
      });
      obj = {
          status: "fail",
          message: "Following fields are required for signing up - " + message,
      };
  }
  res.json(obj);
});


module.exports = app1;