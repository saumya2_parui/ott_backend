const router = require("express").Router();

router.use('/api/', require('./controllers/signup'));
router.use('/api/', require('./controllers/signin'));

module.exports = router;