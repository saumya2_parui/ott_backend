var createError = require('http-errors');
var express = require('express');
var bodyParser = require('body-parser');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var pgp = require('pg-promise')({});
var cors = require('cors');
var https = require('https');
var fs = require('fs');
var crypto = require('crypto');
var app = express();

const route = require("./routes");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());

app.post('/admin_chk', async (req, res, next) => {
  obj = {
      status: "fail",
      message: "Following fields are required for signing up",
  };
res.json(obj);
});

// Import all defined routes
// app.use('/api/', require('./controllers/index'));
console.log("Routes initializing");
app.use("/", route);


var port = 8082;
app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})

module.exports = app;
