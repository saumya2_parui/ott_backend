const sleep = require('util').promisify(setTimeout);
const bcrypt = require('bcrypt');
var pgp = require('pg-promise')({});
var db = pgp({ host: '13.233.117.90', port: 5432, database: 'dbott', user: 'magnox', password: 'DaTaBaSe#123' });
pgp.pg.types.setTypeParser(1114, function (stringValue) {
	return stringValue;
});

function now() {
	let curdate = new Date();
	return curdate.getTime() + curdate.getTimezoneOffset() * 60000;
  }


  //============================
  const findUser = async (param, paramType) => {
	let user, obj;
	let condition;
	let conditionVal;
	// console.log(param, paramType);
	let user_details;
	
if (param && paramType) {
      switch (paramType) {
        case "id":
          condition = " WHERE num_user_id='"+param.id+"'";
          conditionVal = param.num_user_id;
          break;
        case "email":
          condition = " WHERE txt_emailid='"+param.email+"'";
          conditionVal = param.txt_emailid;
          break;
        case "phone":
          condition = " WHERE num_mobile_no='"+param.phone+"'";
          conditionVal = param.num_mobile_no;
          break;
        default:
          return null;
      }

	//   console.log(condition);

        user_details = new Promise(function(resolve, reject) {
          try {
          db.oneOrNone("SELECT * FROM ott_user_dtls "+condition).then(function(data) {
			//   console.log(data);
			if (data != null) {
				obj = { status: "success", result: data };
			} else {
				obj = { status: "fail", result: "" };
			}
            resolve(obj); 
          }).catch (function (e) {
            obj = { status: "fail", result: e };
            resolve(obj);
          });
          }
          catch (e) {
          obj = { status: "fail", result: e };
          resolve(obj);
          }
      });
    } else {
      user_details = { status: "fail", message: "pass searchByValue and searchByType" };
    }
  return user_details;
};




const addUser = async (user_data) => {    
	let hashPassword = await bcrypt.hash(user_data.txt_password, 10);  
	let dat_creation_date = user_data.dat_creation_date;
	let num_master_id = "3";
	let first_name = user_data.first_name;
	let last_name = user_data.last_name;
	let txt_user_id = user_data.txt_user_id;
	let txt_password = hashPassword;
	let txt_gender = user_data.txt_gender;
	let txt_emailid = user_data.txt_emailid;
	let num_mobile_no = user_data.num_mobile_no;
	let txt_active = user_data.txt_active;
	let yn_mobile_verified = "0";
	let yn_email_verified = "0";

	let userdetails = new Promise(function(resolve, reject) {
	  try {
		db.one("INSERT INTO ott_user_dtls (num_master_id,txt_user_id,txt_password,txt_gender,txt_emailid,num_mobile_no,txt_active,dat_creation_date,yn_mobile_verified,yn_email_verified,first_name,last_name) VALUES (${num_master_id},${txt_user_id},${txt_password},${txt_gender},${txt_emailid},${num_mobile_no},${txt_active},${dat_creation_date},${yn_mobile_verified},${yn_email_verified},${first_name},${last_name}) RETURNING *;", { num_master_id, txt_user_id, txt_password, txt_gender, txt_emailid, num_mobile_no, txt_active, dat_creation_date , yn_mobile_verified , yn_email_verified , first_name , last_name }).then(function(data) {
			obj = { status: "success", result: data };
		  	resolve(obj); 
		}).catch (function (e) {
			obj = { status: "fail", result: e };
			resolve(obj);
		});
	  }
	  catch (e) {
		obj = { status: "fail", result: e };
		resolve(obj);
	  }
	});
	return userdetails;
  }

  const signin = async(username, password) =>{
	user_details = new Promise(function(resolve, reject) {
		try {
		db.oneOrNone("SELECT * FROM ott_user_dtls  WHERE txt_user_id='"+ username +"'").then(function(data) {
		  if (data != null) {
			match = bcrypt.compareSync(password, data.txt_password);
			if (match) {
				// await Visitors.findByIdAndUpdate(user.num_user_id, { lastLogin: Date.now() });
				obj = { status: "success", message: "logged_in", result: data };
			} else {
				obj = { status: "fail", message: "invalid_password", result: "" };
			}
		  } else {
			  obj = { status: "fail", message: "not_found" , result: "" };
		  }
		  resolve(obj); 
		}).catch (function (e) {
		  obj = { status: "fail", result: e };
		  resolve(obj);
		});
		}
		catch (e) {
		obj = { status: "fail", result: e };
		resolve(obj);
		}
	});
	return user_details;
}



exports.signin = signin;
exports.addUser = addUser;
exports.findUser = findUser;

